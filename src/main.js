import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from './store'
import '@/style/index.scss' // glob scss
import './plugins/element.js'
import animated from 'animate.css'
import '@/assets/iconfont/iconfont.css'

import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/index.css'

Vue.use(VXETable)
// // 引入样式
// import 'vue-easytable/libs/themes-base/index.css'
// // 导入 table 和 分页组件
// import { VTable, VPagination } from 'vue-easytable'

// // 注册到全局
// Vue.component(VTable.name, VTable)
// Vue.component(VPagination.name, VPagination)

Vue.use(animated)
// import SlideVerify from 'vue-monoplasty-slide-verify'

// Vue.use(SlideVerify)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
