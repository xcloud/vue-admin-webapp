import $axios from './index'


export const FINDONE = params => $axios.post('data/findOne', params)
export const FINDALL = params => $axios.post('data/findAll', params)
export const FINDALLNAME = params => $axios.post('data/findAllName', params)
export const SAVE = params => $axios.post('data/save', params)
export const SAVEORUPDATE = params => $axios.post('data/saveOrUpdate', params)
export const DELETE = params => $axios.post('data/delete', params)
export const CLEAR = params => $axios.post('data/clear', params)

export function getPageTab1(params) {
  const url = '/getPageData1'
  return $axios.get(url, params)
}
export function getPageTab2() {
  const url = '/getPageData2'
  return $axios.get(url)
}
