import $axios from './index'

export const FINDONE = params => $axios.post('schema/findOne', params)
export const FINDALL = params => $axios.post('schema/findAll', params)
export const FINDALLNAME = params => $axios.post('schema/findAllName', params)
export const SAVE = params => $axios.post('schema/save', params)
export const UPDATE = params => $axios.post('schema/update', params)
export const SAVEORUPDATE = params => $axios.post('schema/saveOrUpdate', params)
export const DELETE = params => $axios.post('schema/delete', params)
